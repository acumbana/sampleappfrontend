import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { Fund } from './../../model/fund';
import { FundService } from 'src/app/service/fund.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-fund',
  templateUrl: './add-fund.component.html',
  styleUrls: ['./add-fund.component.css']
})
export class AddFundComponent implements OnInit {
  private fund: Fund;
  
  constructor(private fundService:FundService, private router: Router, private location:Location, private activatedRoute:ActivatedRoute) {
   }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(parameterMap =>{
      const id = +parameterMap.get('id');
      this.getFund(id);
    })
  }

  getFund(id:number){
    if(id===0){
      this.fund = {
        id:null,
        name:null,
        value:null,
        investor:null
      }
    }else{
      this.fundService.read(id).subscribe(data =>{
        this.fund = Object.assign({},data);
      })
    }
    
  }
  onSubmit(){

    const newFund:Fund = Object.assign({}, this.fund);
    this.fundService.create(newFund).subscribe(data =>{
      console.log(data);
    })
      this.router.navigate(['/fund/0']);
  }

  back(){
    this.location.back();
  }

}