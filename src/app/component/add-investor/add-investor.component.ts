import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { Router} from '@angular/router';

import { InvestorWrapper } from '../../model/InvestorWrapper';
import { InvestorService } from 'src/app/service/investor.service';

@Component({
  selector: 'app-add-investor',
  templateUrl: './add-investor.component.html',
  styleUrls: ['./add-investor.component.css']
})
export class AddInvestorComponent implements OnInit {
  private investorIdForm:FormGroup;
  private investorIds:FormArray;
  private investorWrapper:InvestorWrapper = new InvestorWrapper();
  
  
  constructor(private fb:FormBuilder, private router: Router, private investorService:InvestorService) { 
    this.investorIdForm = this.fb.group({
      investorIds:this.fb.array([this.createInvestorId()]),
      name: new FormControl('')
    })
  }

  ngOnInit() {
    for(var i=0;i<2;i++){
      this.addId();
    }
  }

  createInvestorId():FormGroup{
    return this.fb.group({
      id:''
    });
  }

  addId() {
    this.investorIds = this.investorIdForm.get('investorIds') as FormArray;
    this.investorIds.push(this.createInvestorId());

  }
  removeId(i: number) {
    this.investorIds.removeAt(i);
  }
  get idControls() {
    return this.investorIdForm.get('investorIds')['controls'];
  }

  submit(){
    this.investorWrapper.investor.name = this.investorIdForm.get('name').value;

    var controls = (<FormArray>this.investorIdForm.get('investorIds'));

    for(var i=0;i<controls.length;i++){
      if(controls.at(i).get('id').value){
        this.investorWrapper.fundId.push(controls.at(i).get('id').value);
      }
    }

    this.investorService.create(this.investorWrapper).subscribe(data=>{

    },
      err =>{
        alert('An error has ocurred, funds with this id might not exist');
      }
    );
    this.investorWrapper.fundId = [];
    this.router.navigate(['/investor/0']);
  }

}
