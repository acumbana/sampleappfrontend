import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../service/client.service';
import { Router, NavigationEnd } from '@angular/router';
import { Client } from 'src/app/model/client';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css'],

})
export class ClientComponent implements OnInit {

  private dataSource = [];
  navigationSubscription;

  constructor(private clientService : ClientService, private router:Router){
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.reloadData();
      }
    });
  };

  ngOnInit() {
    this.reloadData();
  }

  reloadData(){
    this.clientService.getAll().subscribe(
      data => {
        this.dataSource = data;
      }
    )
  }
  
  selectClient(client:Client){
    this.router.navigate(['/investor/',client.id]);
    console.log(client.id);
  }

  edit(client:Client){
    this.router.navigate(["/client/edit",client.id])
  }
}