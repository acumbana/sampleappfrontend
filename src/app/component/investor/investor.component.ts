import { Component, OnInit } from '@angular/core';
import { InvestorService } from 'src/app/service/investor.service';
import { Investor } from 'src/app/model/investor';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-investor',
  templateUrl: './investor.component.html',
  styleUrls: ['./investor.component.css'],

})
export class InvestorComponent implements OnInit {

  private dataSource = [];
  navigationSubscription;

  constructor(private investorService: InvestorService, private router: Router, private activatedRoute: ActivatedRoute) {
    
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.reloadData();
      }
    });

  };

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.activatedRoute.paramMap.subscribe(parameterMap => {
      const id = +parameterMap.get('id');
      this.getInvestor(id);
    });
  }

  getInvestor(id: number) {
    if (id === 0) {
      this.investorService.getAll().subscribe(data => {
        this.dataSource = data;
      })
    } else {
      this.investorService.getInvestorByClientId(id).subscribe(data => {
        this.dataSource = data;
        console.log(this.dataSource);
      })
    }
  }

  selectInvestor(investor: Investor) {
    this.router.navigate(['/fund/', investor.id]);
    console.log(investor.id);
  }

  edit(investor: Investor) {
    this.router.navigate(["/investor/edit", investor.id])
  }
}