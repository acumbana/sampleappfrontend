import { Component, OnInit } from '@angular/core';
import { FundService } from 'src/app/service/fund.service';
import { Fund } from 'src/app/model/fund';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-fund',
  templateUrl: './fund.component.html',
  styleUrls: ['./fund.component.css'],
})
export class FundComponent implements OnInit {
  private dataSource = [];
  navigationSubscription;

  constructor(private fundService: FundService, private router: Router, private route: ActivatedRoute, private activatedRoute: ActivatedRoute) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.reloadData();
      }
    });
  };

  ngOnInit() {

  }

  reloadData() {
    this.activatedRoute.paramMap.subscribe(parameterMap => {
      const id = +parameterMap.get('id');
      this.getFunds(id);
    });
  }

  getFunds(id: number) {
    if (id === 0) {
      this.fundService.getAll().subscribe(data => {
        this.dataSource = data;
      });
    } else {
      this.fundService.getFundByInvestorId(id).subscribe(data => {
        this.dataSource = data;
        console.log(data);
      })
    }

  }

  delete(fund: Fund) {
    this.fundService.delete(fund.id);
    this.dataSource.splice(this.dataSource.indexOf(fund, 1));
    this.router.navigate(['/fund/0']);
  }

  edit(fund: Fund) {
    this.router.navigate(["/fund/edit", fund.id])
  }
}