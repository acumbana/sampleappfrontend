import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { ClientWrapper } from '../../model/clientWrapper'
import { ClientService } from 'src/app/service/client.service';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {
  private clientIdForm:FormGroup;
  private clientIds:FormArray;
  private clientWrapper:ClientWrapper;
  

  constructor(private fb:FormBuilder, private router: Router , private clientService:ClientService) { 
    this.clientWrapper = new ClientWrapper();
    this.clientWrapper.client.name='name';
    this.clientIdForm = this.fb.group({
      clientIds:this.fb.array([this.createClientId()]),
      name: new FormControl(''),
      description: new FormControl('')
    })
  }

  ngOnInit() {
    for(var i=0;i<2;i++){
      this.addId();
    }
  }

  createClientId():FormGroup{
    return this.fb.group({
      id:''
    });
  }

  addId() {
    this.clientIds = this.clientIdForm.get('clientIds') as FormArray;
    this.clientIds.push(this.createClientId());

  }
  removeId(i: number) {
    this.clientIds.removeAt(i);
  }
  get idControls() {
    return this.clientIdForm.get('clientIds')['controls'];
  }

  submit(){
    this.clientWrapper.client.name = this.clientIdForm.get('name').value;
    this.clientWrapper.client.description = this.clientIdForm.get('description').value;

    var controls = (<FormArray>this.clientIdForm.get('clientIds'));

    for(var i=0;i<controls.length;i++){
      if(controls.at(i).get('id').value){
        this.clientWrapper.investorId.push(controls.at(i).get('id').value);
      }
    }

    this.clientService.create(this.clientWrapper).subscribe( data=>{
    },
      err =>{
        alert('An error has occurred, Investor with this id might not exist');
      }
    )

    this.clientWrapper.investorId = [];
    this.router.navigate(['/client/0']);
  }

}
