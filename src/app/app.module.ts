import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, ExtraOptions } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { ClientComponent } from './component/client/client.component';
import { AddInvestorComponent } from './component/add-investor/add-investor.component';
import { InvestorComponent } from './component/investor/investor.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatExpansionModule} from '@angular/material/expansion';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { FundComponent } from './component/fund/fund.component';
import { AddFundComponent } from './component/add-fund/add-fund.component';
import { AddClientComponent } from './component/add-client/add-client.component';


const routes: Routes =[
  {path:'investor/edit/:id', component:AddInvestorComponent},
  {path:'investor/:id', component: InvestorComponent, runGuardsAndResolvers: "always"},
  {path: 'client/:id', component: ClientComponent, runGuardsAndResolvers: "always"},
  {path: 'fund/:id', component: FundComponent},
  {path:'fund/edit/:id', component: AddFundComponent, runGuardsAndResolvers: "always"},
  {path: 'client/edit/:id', component: AddClientComponent},
  { path: '**', redirectTo:'/client/0'}
]

const options: ExtraOptions = {
  onSameUrlNavigation: 'reload'
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ClientComponent,
    AddInvestorComponent,
    InvestorComponent,
    FundComponent,
    AddFundComponent,
    AddClientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes, options),
    BrowserAnimationsModule,
    MatExpansionModule,
    HttpClientModule,
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
