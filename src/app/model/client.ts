import {Investor} from './investor';

export class Client{
    id:number;//clients id
	name:string;//clients name
	description:string;//description
	investor:Investor[];//array of investors
}