import {Client} from './client';
import {Fund} from './fund';

export class Investor{
	id:number;// investor's primary key
	name:string=''; // investor's name 
	fund:Fund[];// One investor can have mutiple funds
    client:Client;	//One investor can only be represented by one client
}