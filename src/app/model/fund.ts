import {Investor} from './investor';

export class Fund{
    id:number;
	name:string; 
	value:number;
    investor:Investor;
}