import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { InvestorWrapper } from '../model/InvestorWrapper';

let API_URL = "http://localhost:8080/investor/"
@Injectable({
  providedIn: 'root'
})
export class InvestorService {

  constructor(private http:HttpClient) { }

  create(investorWrapper:InvestorWrapper) : Observable<any>{
    console.log(investorWrapper.investor.name);

    return this.http.post(API_URL, JSON.stringify(investorWrapper), {headers: {"Content-type":"application/json; charset=UTF-8"}});
  }

  read(id:number) : Observable<any>{
    return this.http.get(`${API_URL}/${id}`);
  }

  update(id:number, investorWrapper: InvestorWrapper) : Observable<any>{
    return this.http.post(`${API_URL}/${id}`,JSON.stringify(investorWrapper), {headers: {"Content-type":"application/json; charset=UTF-8"}});
  }

  getAll() : Observable<InvestorWrapper[]>{
    return this.http.get<InvestorWrapper[]>(API_URL);
  }

  getInvestorByClientId(id :number) : Observable<any>{
    return this.http.get(`${API_URL}/client/${id}`);
  }

}
