import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Fund } from '../model/fund';

let API_URL = "http://localhost:8080/fund"

@Injectable({
  providedIn: 'root'
})
export class FundService {

  private deleteStatus = new Observable;
  constructor(private http:HttpClient) { }

  create(fund:Fund) : Observable<any>{
    return this.http.post(API_URL, JSON.stringify(fund),
    {headers: {"Content-type":"application/json; charset=UTF-8"}});
  }

  read(id:number) : Observable<any>{
    return this.http.get(`${API_URL}/${id}`);
  }

  update(id:number, fund: Fund) : Observable<any>{
    return this.http.put(`${API_URL}/${id}`,JSON.stringify(fund), {headers: {"Content-type":"application/json; charset=UTF-8"}});
  }

  getAll() : Observable<Fund[]>{
    return this.http.get<Fund[]>(API_URL);
  }

  getFundByInvestorId(id :number) : Observable<Fund[]>{
    return this.http.get<Fund[]>(`${API_URL}/investor/${id}`);
  }

  delete(id:number){
    return this.http.delete(`${API_URL}/${id}`).subscribe(data=>{
    });
  }
}
