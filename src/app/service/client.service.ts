import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import { ClientWrapper } from '../model/clientWrapper';
import { Client } from '../model/client';

let API_URL = "http://localhost:8080/client";
@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http:HttpClient) {

   }

  create(clientWrapper:ClientWrapper) : Observable<any>{
    console.log(JSON.stringify(clientWrapper));
    return this.http.post(API_URL, JSON.stringify(clientWrapper),
    {headers: {"Content-type":"application/json; charset=UTF-8"}});
  }

  read(id:number) : Observable<any>{
    return this.http.get(`${API_URL}/${id}`);
  }

  update(id:number, clientWrapper: ClientWrapper) : Observable<any>{
    return this.http.put(`${API_URL}/${id}`,JSON.stringify(clientWrapper), {headers: {"Content-type":"application/json; charset=UTF-8"}});
  }

  getAll() : Observable<Client[]>{
    return this.http.get<Client[]>(API_URL);
  }
}
